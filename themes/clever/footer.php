	<!-- About Us  -->
	
	<div class="container-fluid about">
		<div class="container ">
			<div class="row">
				<div class="col-md-3">
					<h3>About Us</h3>
					<?php if(!dynamic_sidebar('aboutus')):?>
						<span><h3>Place to About Us</h3></span>
					<?php endif;?>
				</div>
				<div class="col-md-3">
					<h3>Recent Posts</h3>
					<?php if(!dynamic_sidebar('recent_posts')):?>
						<span><h3>Place to Recent posts</h3></span>
					<?php endif;?>
				</div>
				<div class="col-md-3">
						<h3>Tag Cloud</h3>
					<?php if(!dynamic_sidebar('tag_cloud')):?>
						<span><h3>Place to Tag Cloud</h3></span>
					<?php endif;?>
				</div>
				<div class="col-md-3">
					<h3>Recent Posts</h3>
					<?php if(!dynamic_sidebar('recent_posts')):?>
						<span><h3>Place to Recent posts</h3></span>
					<?php endif;?>
				</div>
			</div>
		</div>	
	</div>
	
	<!-- /About Us  -->
	
	<!-- Footer  -->
	
	<div class="container-fluid footer marg">
		<div class="container ">
			<div class="row">
				<div class="col-md-6">
					<p>Theme By GoodLayers</p>
				</div>
				<div class="col-md-6">
					<p class="content-right">Copyright © 2014 - All Right Reserved - GoodLayers inc</p>
				</div>
				
			</div>
		</div>	
	</div>
	
	<!-- /Footer  -->
  

    
	<script>
		$(document).ready(function(){
			$('.bxslider').bxSlider({
			  nextSelector: '#slider-next',
			  prevSelector: '#slider-prev',
			  nextText: '<img src="<?php bloginfo('template_url')?>/img/right.png">',
			  prevText: '<img src="<?php bloginfo('template_url')?>/img/left.png">',
			  adaptiveHeight: true,
			  pager: false,
				slideWidth: 400,
				minSlides: 2,
				maxSlides: 3,
				slideMargin: 10
			});
			$('.bxslider2').bxSlider({
			  nextSelector: '#slider-next2',
			  prevSelector: '#slider-prev2',
			  nextText: '<img src="<?php bloginfo('template_url')?>/img/right.png">',
			  prevText: '<img src="<?php bloginfo('template_url')?>/img/left.png">',
			  adaptiveHeight: true,
			  pager: false,
				slideWidth: 800,
				minSlides: 1,
				maxSlides: 1,
				slideMargin: 10
			});
			$('.bxslider3').bxSlider({
			  nextSelector: '#slider-next3',
			  prevSelector: '#slider-prev3',
			  nextText: '<img src="<?php bloginfo('template_url')?>/img/right.png">',
			  prevText: '<img src="<?php bloginfo('template_url')?>/img/left.png">',
			  adaptiveHeight: true,
			  pager: false,
				slideWidth: 800,
				minSlides: 1,
				maxSlides: 1,
				slideMargin: 10
			});
		});
	</script>
	<?php wp_footer();?> 
  </body>
</html>