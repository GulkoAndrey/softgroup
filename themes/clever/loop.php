<?php if ( have_posts() ) : ?>
<?
if ( 'instructor-list-style-1' == get_the_title() ) { ?>
	<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 bg_page_instr">
			<div class="clever-overlay"></div>
			<h3>Instructor List Style 1</h3>
		</div>
	</div>
	</div>
	<div class="container cr">
		<div class="row">
			
<?
	$args = array(
		'posts_per_page' => 3,
		'post_type'   => 'team',
		'order'       => 'DESC'
	);
	$team = get_posts( $args );
	
	foreach ( $team as $human ) { ?>
		<div class="col-md-4">
			<div>
				<div class="img-circle">
					<?php echo get_the_post_thumbnail( $human->ID, 'thumbnail' ); ?>
				</div>
				<div style="text-align:center;">
					<h3><?php echo $human->post_title ?></h3>
					<h4><?php echo get_post_meta($human->ID, 'posada', true);?></h4>
					<p><?php echo $human->post_content ?></p>
					<a href="<?php echo get_permalink( $human->ID ); ?>"><button type="button" class="btn btn-success">VIEW PROFILE</button></a>
				</div>
			</div>
		</div>
	<? } ?>
		</div>
	</div>
	
<? } ?>
<? if ( '404' == get_the_title() ) { ?>
	
	
		<div class="container-fluid">
			<div class="row">
		<div class="col-md-12 bg_page_instr ">
			<div class="clever-overlay"></div>
			<h3>404</h3>
		</div>
	</div>
	</div>
	<div class="container cr">
		<div class="row">

		
		<div class="col-md-3">
		</div>
		<div class="col-md-6">
			<div class="nopage">
				<div class="input-group">
				  <input type="text" class="form-control" placeholder="Search for...">
				  <span class="input-group-btn">
					<button class="btn btn-default" type="button">Go!</button>
				  </span>
				</div><!-- /input-group -->
			</div>
		</div>
	
		</div>
	</div>
	
	
<? } ?>



<?php endif; ?>