<?php
/**
 * Підключаємо локалізацію шаблону
 */
load_theme_textdomain('clever', get_template_directory() . '/languages');

/**
 * Додаємо favicon, charset, viewport
 */
function add_head_meta_tags()
{
    ?>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
}
add_action('wp_head', 'add_head_meta_tags');


/**
* Підключаємо скрипти та css-стилі
*/
function load_style_script(){
	/*wp_enqueue_script("jquery");*/
	wp_enqueue_script('jquery2', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js');
	wp_enqueue_script('bootstrapchyk', get_template_directory_uri().'/js/bootstrap.min.js');
	wp_enqueue_script('bxslider', get_template_directory_uri().'/js/jquery.bxslider.min.js');
	
	wp_enqueue_style('bootstrapchyk', get_template_directory_uri().'/css/bootstrap.min.css');
	wp_enqueue_style('style', get_template_directory_uri().'/css/style.css');
	wp_enqueue_style('bxslider', get_template_directory_uri().'/css/jquery.bxslider.css');
	
	
}
add_action('wp_enqueue_scripts','load_style_script');

/**
* Додаємо віджети
*/

add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
	register_sidebar(array(
		'name' => __( 'Меню', 'clever' ),
		'id' => 'menu_header',
		'description' => __( 'Widgets in this area will be shown Menu.', 'clever' ),
		'class' => '',
		'before_widget' =>'',
		'after_widget' =>'',
	));
		
	register_sidebar(array(
		'name' => __( 'Recent News', 'clever' ),
		'id' => 'recent_news',
		'description' => __( 'Widgets in this area will be shown Recent News.', 'clever' ),
		'class' => '',
		'before_widget' =>'',
		'after_widget' =>'',
	));
	
	register_sidebar(array(
		'name' => __( 'Recent Post', 'clever' ),
		'id' => 'recent_posts',
		'description' => __( 'Widgets in this area will be shown Recent Post.', 'clever' ),
		'class' => '',
		'before_widget' =>'',
		'after_widget' =>'',
	));
	
	
    register_sidebar( array(
        'name' => __( 'Sidebar for School Walkthrough', 'clever' ),
        'id' => 'sw-sidebar',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'clever' ),
        'before_widget' => '',
	'after_widget'  => '',
	'before_title'  => '',
	'after_title'   => '',
    ) );
	
	
	register_sidebar( array(
        'name' => __( 'Featured Instructer', 'clever' ),
        'id' => 'f-instructor',
        'description' => __( 'Widgets in this area for Featured Instructer .', 'clever' ),
        'before_widget' => '',
	'after_widget'  => '',
	'before_title'  => '',
	'after_title'   => '',
    ) );
	
	register_sidebar( array(
        'name' => __( 'About Us', 'clever' ),
        'id' => 'aboutus',
        'description' => __( 'Widgets in this area for About Us .', 'clever' ),
        'before_widget' => '',
	'after_widget'  => '',
	'before_title'  => '',
	'after_title'   => '',
    ) );
	
	register_sidebar( array(
        'name' => __( 'Tag Cloud', 'clever' ),
        'id' => 'tag_cloud',
        'description' => __( 'Widgets in this area for Tag Cloud .', 'clever' ),
        'before_widget' => '',
	'after_widget'  => '',
	'before_title'  => '',
	'after_title'   => '',
    ) );
	
	register_sidebar( array(
        'name' => __( 'Social Buttons', 'clever' ),
        'id' => 'soc_buttons',
        'description' => __( 'Widgets in this area for Social Buttons .', 'clever' ),
        'before_widget' => '',
	'after_widget'  => '',
	'before_title'  => '',
	'after_title'   => '',
    ) );
	
	
	
}

	
/**
 *  Додаємо віджет останніх постів типу news
 */

add_action('widgets_init', 'last_posts');
function last_posts (){
    register_widget('WFM_Widget');
}

class WFM_Widget extends WP_Widget{
    function __construct()
    {
        parent::__construct('FW', 'Последние записи определенного типа', array( 'description' => 'Вывод последних записей постов конкретного типа'));
    }
    function widget($args,$instance)
    {
        //var_dump($instance);
        extract($args);
        extract($instance);

        echo $before_widget;
        echo $before_title.$title.$after_title;
        //echo '<div>'.$category.'</div>';

        // The Query
        $the_query = new WP_Query( array( 'post_type' => $posttype, 'posts_per_page' => $limit, 'order'   => 'DESC' ) );

// The Loop
        if ( $the_query->have_posts() ) {
				echo '<h3>Recent News <span class="toblog"><a href="#">/ Read The Blog</a></span></h3>';
           echo '<div>';
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
				echo "<div class='post_decor'>";
				echo "<div class='left-part-post'>";
				the_post_thumbnail();
				echo "</div>";
				echo '<div class="right-part-post">';
                echo '<a href="'.get_permalink().'">' . get_the_title() . '</a>';
				echo '<p class="datepost">'.get_the_date().'</p>';
				echo "</div>";
				echo "<div class='clearfix'></div>";
				echo '</div><br>';
            }
			echo '</div>';
            /* Restore original Post Data */
            wp_reset_postdata();
        } else {
            // no posts found
        }


        echo $after_widget;


    }

    function form($instance){

        //$category = null;
        $limit = 10;
        // если instance не пустой, достанем значения

        $posttypeId = $this->get_field_id("posttype");
        $posttypeName = $this->get_field_name("posttype");

        $limitId = $this->get_field_id("limit");
        $limitName = $this->get_field_name("limit");


        extract($instance);
        ?>
        <p>
            <label for="<?=$this->get_field_id('title');?>">Заголовок</label>
            <input type="text" name="<?=$this->get_field_name('title');?>" id="<?=$this->get_field_id('title');?>" value="<? if($title) echo esc_attr($title);?>" class="widefat">
        </p>

        <p>
            <label for="<?= $posttype ?>"><?= __('posttype', 'sg') ?></label>
        </p>
        <p>
            <select class="widefat" name="<?= $posttypeName ?>" id="<?= $posttypeName ?>">
                <?php $posttypes = get_post_types('','names'); ?>
                <?php foreach ($posttypes as $post_type){ ?>
                    <option value="<?= $post_type ?>"<?php if($posttype == $post_type){ ?> selected="selected"<?php } ?>><?= $post_type ?></option>
                <?php } ?>
            </select>
        </p>

        <p>
            <label for="<?= $limitId ?>"><?= __('Limit', 'sg') ?></label>
        </p>
        <p>
            <input class="widefat" type="number" min="2" id="<?= $limitId ?>" name="<?= $limitName ?>" value="<?= $limit ?>">
        </p>
        <?
    }
}	
	
	
/**
 * Підключаємо підтримку мініатюр
 */
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 150, 150 );
}	
/**
* Додаємо новий тип - слайдер
*/
function slider_posts(){  
	$labels = array(  
		'name' => __( 'Слайдери', 'clever' ), // Основное название типа записи  
		'singular_name' => __( 'Слайдер', 'clever' ), // отдельное название записи типа Book  
		'add_new' => __( 'Додати новий', 'clever' ),  
		'add_new_item' => __( 'Додати новий Слайдер', 'clever' ),  
		'edit_item' => __( 'Редагувати Слайдер', 'clever' ),  
		'new_item' => __( 'Новий слайдер', 'clever' ),  
		'view_item' => __( 'Переглянути слайдер', 'clever' ),  
		'search_items' => __( 'Знайти Слайдер', 'clever' ), 
		'not_found' =>  __( 'Знайти Слайдер', 'clever' ),  
		'not_found_in_trash' => __( 'В корзині слайдера не знайдено', 'clever' ),  
		'parent_item_colon' => '',  
		'menu_name' => __( 'Слайдери', 'clever' )  
	);  
	$args = array(  
		'labels' => $labels,  
		'public' => true,  
		'publicly_queryable' => true,  
		'show_ui' => true,  
		'show_in_menu' => true,  
		'query_var' => true,  
		'rewrite' => true,  
		'capability_type' => 'post',  
		'has_archive' => true,  
		'hierarchical' => false,  
		'menu_position' => null,  
		'supports' => array('title', 'thumbnail','editor')  
	);  
	register_post_type('slider', $args);  
}
add_action('init', 'slider_posts');

/**
* Додаємо новий тип - ярлик
*/
function yarlyk_posts(){ 
    $labels = array(
        "name" => __( 'Ярлики', 'clever' ),
        "singular_name" => __( 'Ярлик', 'clever' ),
    );

    $args = array(
        "label" => __( 'Ярлики', 'clever' ),
        "labels" => $labels,
        "description" => "__( 'Пости типу Ярлики', 'clever' )",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "yarlyk", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail", "custom-fields" ),
    );

    register_post_type( "yarlyk", $args );
}

add_action( 'init', 'yarlyk_posts' );

/*тип ярлик*/

function get_totalposts() {
	global $wpdb;
	$totalposts = intval($wpdb->get_var("SELECT COUNT(ID) FROM $wpdb->posts WHERE post_type = 'slider' AND post_status = 'publish'"));

	return $totalposts;
}

/**
* Додаємо новий тип - Courses
*/
function courses_posts(){ 
    $labels = array(
        "name" => __( 'Курси', 'clever' ),
        "singular_name" => __( 'Курс', 'clever' ),
    );

    $args = array(
        "label" => __( 'Курси', 'clever' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "course", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail", "custom-fields" ),
    );

    register_post_type( "cource", $args );
}

add_action( 'init', 'courses_posts' );

/*тип Courses*/

/**
* Додаємо новий тип - News
*/
function news_posts(){ 
    $labels = array(
        "name" => __( 'Новини', '' ),
        "singular_name" => __( 'Новина', '' ),
    );

    $args = array(
        "label" => __( 'Новини', 'clever' ),
        "labels" => $labels,
        "description" => "__( 'Пости типу Новини', 'clever' )",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "news", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail", "custom-fields" ),
    );

    register_post_type( "news", $args );
}

add_action( 'init', 'news_posts' );

/*тип Citata*/

function news_citats(){ 
    $labels = array(
        "name" => __( 'Цитати', 'clever' ),
        "singular_name" => __( 'Цитати', 'clever' ),
    );

    $args = array(
        "label" => __( 'Цитати', 'clever' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "citaty", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail", "custom-fields" ),
    );

    register_post_type( "citaty", $args );
}

add_action( 'init', 'news_citats' );

/*тип Posts*/


/*тип Team*/

function new_teams(){ 
    $labels = array(
        "name" => __( 'Команда', 'clever' ),
        "singular_name" => __( 'Команда', 'clever' ),
    );

    $args = array(
        "label" => __( 'Команда', 'clever' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "team", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail", "custom-fields" ),
    );

    register_post_type( "team", $args );
}

add_action( 'init', 'new_teams' );

/*тип Team*/


/*Реєструємо опції*/

/*delete_option("myhack_extraction_length", '255', 'Максимальное количество букв в распакованном тексте.', 'yes'); */

add_action('admin_init','contacts_options');

function contacts_options(){
	register_setting('general','telefone_option');
	register_setting('general','email_option');
	
	add_settings_field(
	'telefone_option',
	__('Телефон в шапці', 'clever'),
	'option_cb',
	'general'
	);
	add_settings_field(
	'email_option',
	__('Електронна пошта в шапці', 'clever'),
	'option_cb2',
	'general'
	);
	
}
function option_cb(){
	?>
	<input type="text" name='telefone_option' id="telefone_option" value="<?php echo esc_attr(get_option('telefone_option')); ?>" class="regular-text"
	">
	<?php
	
};
function option_cb2(){
	?>
	<input type="text" name='email_option' id="email_option" value="<?php echo esc_attr(get_option('email_option')); ?>" class="regular-text"
	">
	<?php
	
};
/*Реєструємо опції*/
	
	
?>