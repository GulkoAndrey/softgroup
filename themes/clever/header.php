<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php bloginfo('name');?></title>
	<?php wp_head();?>
  </head>
  <body>
	<div class="container">
		<div class="row contacts">
			<div class="col-md-2">
				<span class="glyphicon glyphicon-earphone"></span>
				 <?php echo esc_attr(get_option('telefone_option')); ?>
			</div>
			<div class="col-md-4">
				<span class="glyphicon glyphicon-envelope"></span>
				 <a href="mailto:<?php echo esc_attr(get_option('email_option')); ?>"><?php echo esc_attr(get_option('email_option'));?> </a> 
				</div>
				
			<div class="col-md-4 top-social-wrapper">
				<?php if(!dynamic_sidebar('soc_buttons')):?>
					<span>Place to Social Buttons</span>
				<?php endif;?>
			</div>
			<div class="col-md-2">
				<span class="glyphicon glyphicon-lock"></span>
				<a href="#">Sign In</a> | <a href="#">Sign Up</a>
			</div>
		</div>
	</div>
	
	<!-- Line  -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 line">
			</div>
		</div>
	</div>
	<!-- /Line  -->
	
	<!-- Menu  -->
	<div class="container">
		<div class="row menus">
			<div class="col-md-3 logo">
				<a href="index.php"><img src="<?php bloginfo('template_url')?>/img/logo.png"></a>
			</div>
			<div class="col-md-8 ">
				<ul class="main-menu ">
					<?php if(!dynamic_sidebar('menu_header')):?>
						<span>Головне меню</span>
					<?php endif;?>
					</ul>
				<!--<ul class="main-menu ">
					<li><a href="#">HOME</a></li>
					<li><a href="#">LMS</a></li>
					<li><a href="#">COURSES</a></li>
					<li><a href="#">PAGES</a></li>
					<li><a href="#">BLOG</a></li>
					<li><a href="#">PORTFOLIO</a></li>
					<li><a href="#">SHORTCODES</a></li>
				</ul>-->
				
			</div>
			<div class="col-md-1 search">
				<span class="glyphicon glyphicon-search"></span>

			</div>
		</div>
	</div>
	<!-- /Menu  -->