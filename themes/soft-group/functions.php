<?php

/**
 * Підключаємо локалізацію шаблону
 */
load_theme_textdomain('sg', get_template_directory() . '/languages');


/**
 * Додаємо favico, charset, viewport
 */
function add_head_meta_tags()
{
    ?>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favico.png" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
}
add_action('wp_head', 'add_head_meta_tags');


/**
 * Реєструємо місце для меню
 */

register_nav_menus( array(
    'primary' => __('Primary', 'sg'),
) );


/**
 * Реєструємо сайдбари теми
 */
 add_action( 'widgets_init', 'register_my_widgets' );
function register_my_widgets() )
{
    register_sidebars(1, array(
        'id' => 'left',
        'name' => __('Left sidebar', 'sg'),
        'description' => '',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));

    register_sidebars(1, array(
        'id' => 'copyright',
        'name' => __('Copyright', 'sg'),
        'description' => '',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<p class="widget-title">',
        'after_title' => '</p>'
    ));
}






/**
 * Підключаємо підтримку мініатюр
 */
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 150, 150 );
}
/**
 * Можемо добавити різні розміри для картинок
 */
if ( function_exists( 'add_image_size' ) ) { 
    //add_image_size( 'photo', 148, 148, true );
}


/**
 * Реєструємо формати постів
 */
function add_post_formats(){
    add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-background' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'add_post_formats', 11 );


/**
 * Замінюємо стандартне закінчення обрізаного тексту з [...] на ...
 */
function custom_excerpt_more( $more ) {
	return ' ...';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );


/**
 * Замінюємо стандартну довжину обрізаного тексту
 */
function custom_excerpt_length( $length ) {
	return 200;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


/**
 * Підключаємо javascript файли
 */
function add_theme_scripts(){
    wp_enqueue_script("jquery");
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') );
    wp_enqueue_script( 'init', get_template_directory_uri() . '/js/init.js', array('jquery', 'bootstrap') );
    wp_enqueue_script( 'generator', get_template_directory_uri() . '/js/generator.js', array('jquery') );
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


/**
 * Підключаємо css файли
 */
function add_theme_style(){
    wp_enqueue_style( 'fonts', get_template_directory_uri() . '/fonts/stylesheet.css');
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style( 'blog-home', get_template_directory_uri() . '/css/blog-home.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style( 'font', get_template_directory_uri() . '/font.css');
}
add_action( 'wp_enqueue_scripts', 'add_theme_style' );

/* add_action('test', 'test');
function test( $a ){
    return '1';
}

add_filter('test', 'test2');
function test2( $a ){
    return '2';
} */

class CustomTaxonomies extends WP_Widget
{
    public function __construct() {
        parent::__construct('custom_taxonomies', __('Custom Taxonomies', 'sg'), array('description' => 'Custom Taxonomies'));
        add_filter('custom_taxonomies_template', [$this, 'getTaxonomyItemTemplate']);
    }

    public function form($instance) {
        $title = "";
        $taxonomy = null;
        $limit = 10;
        // если instance не пустой, достанем значения
        if (!empty($instance)) {
            $title = $instance["title"];
            $taxonomy = $instance["taxonomy"];
            $limit = $instance['limit'];
        }

        $titleId = $this->get_field_id("title");
        $titleName = $this->get_field_name("title");

        $taxonomyId = $this->get_field_id("taxonomy");
        $taxonomyName = $this->get_field_name("taxonomy");

        $limitId = $this->get_field_id("limit");
        $limitName = $this->get_field_name("limit");
        ?>
        <p>
            <label for="<?= $titleId ?>"><?= __('Title', 'sg') ?></label>
        </p>
        <p>
            <input class="widefat" type="text" id="<?= $titleId ?>" name="<?= $titleName ?>" value="<?= $title ?>">
        </p>
        <p>
            <label for="<?= $taxonomyId ?>"><?= __('Taxonomy', 'sg') ?></label>
        </p>
        <p>
            <select class="widefat" name="<?= $taxonomyName ?>" id="<?= $taxonomyId ?>">
                <?php foreach (get_taxonomies(['show_ui' => true, 'show_in_nav_menus' => true, '_builtin' => true], 'object') as $taxonomyItem){ ?>
                    <option value="<?= $taxonomyItem->name ?>"<?php if($taxonomy == $taxonomyItem->name){ ?> selected="selected"<?php } ?>><?= $taxonomyItem->label ?></option>
                <?php } ?>
            </select>
        </p>
        <p>
            <label for="<?= $limitId ?>"><?= __('Limit', 'sg') ?></label>
        </p>
        <p>
            <input class="widefat" type="number" min="2" id="<?= $limitId ?>" name="<?= $limitName ?>" value="<?= $limit ?>">
        </p>
        <?php
    }

    public function update($newInstance, $oldInstance) {
        $values = array();
        $values["title"] = htmlentities($newInstance["title"]);
        $values["taxonomy"] = htmlentities($newInstance["taxonomy"]);
        $values["limit"] = htmlentities($newInstance["limit"]);
        return $values;
    }

    public function widget($args, $instance) {
        $terms = get_terms([
            'taxonomy' => [ $instance['taxonomy'] ],
            'number' => $instance['limit']
        ]);
        $termsLists = [
            0 => [],
            1 => []
        ];
        foreach ($terms as $key => $term){
            if( $key % 2 == 0 ){
                $termsLists[0][] = $term;
            } else {
                $termsLists[1][] = $term;
            }
        }
        ?>
        <?= $args['before_widget'] ?>
        <?= $args['before_title'] ?><?= $instance['title'] ?><?= $args['after_title'] ?>
        <div class="row">
            <div class="col-lg-6">
                <ul class="list-unstyled">
                    <?php foreach ($termsLists[0] as $term){
                        echo apply_filters('custom_taxonomies_template', $term);
                        //echo $this->getTaxonomyItemTemplate( $term );
                    } ?>
                </ul>
            </div>
            <!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <ul class="list-unstyled">
                    <?php foreach ($termsLists[1] as $term){
                        echo $this->getTaxonomyItemTemplate( $term );
                    } ?>
                </ul>
            </div>
            <!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->
        <?= $args['after_widget'] ?>
        <?php
    }

    public function getTaxonomyItemTemplate( $term )
    {
        return '<li><a href="' . get_term_link($term) . '">' . $term->name . '</a></li>';
    }
}





add_action("widgets_init", function () {
    register_widget("CustomTaxonomies");
});

add_filter('custom_taxonomies_template', 'test', 100);

function test( $string ){
    return '<div class="test">' . $string . '</div>';
}

remove_filter('custom_taxonomies_template', 'test', 100);




add_filter( 'the_content', 'my_the_content_filter', 1000 );

function my_the_content_filter( $content ) {
	include_once get_template_directory() . '/inc/simplehtmldom_1_5/simple_html_dom.php';
    $html = str_get_html($content);
    foreach($html->find('a') as $a){
        $currentUrl = parse_url($a->href);
        if( !isset($currentUrl['host']) || $currentUrl['host'] == $siteUrl['host'] ){
            continue;
        }
        $a->target = '_blank';
        $a->href = '/redirect?to=' . $a->href;
    }
    return $html;
}

add_action('wp_loaded', 'redirectLinks');
function redirectLinks(){
    if( strpos($_SERVER['REQUEST_URI'], '/redirect') === 0 && isset($_GET['to'])){
        wp_redirect( $_GET['to'] );
        exit;
    }
};

/**
 *  Додаємо соц. кнопки
 */

add_filter('the_content', 'addSocialShareLinks', 1010);
function addSocialShareLinks( $content ){
    global $post;
    $params = [
        '{url}',
        '{img}',
        '{title}',
        '{desc}',
        '{app_id}',
        '{redirect_url}',
        '{via}',
        '{hashtags}',
        '{provider}',
        '{is_video}'
    ];

    $values = [
        get_permalink(),
        get_the_post_thumbnail_url('full'),
        get_the_title(),
        $post->post_excerpt,
        '', //'{app_id}'
        get_site_url(),
        '', //'{via}'
        '', //{hashtags}
        '', //{provider}
        '', //{is_video}
    ];

    $links = [
        'facebook' => 'https://www.facebook.com/sharer.php?u={url}',
        'twitter' => 'https://twitter.com/intent/tweet?url={url}&text={title}&via={via}&hashtags={hashtags}',
        'google' => 'https://plus.google.com/share?url={url}',
        //'VK' => 'http://vk.com/share.php?url={url}',
        //'OKru' => 'https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl={url}&title={title}',
    ];

    $linksString = '';
    foreach ($links as $key => $link){
       $linksString .= '<a class="soc ' . $key . '" href="' . str_replace($params, $values, $link) . '" target="_blank">' . $key . ' </a>';
       // $linksString .= '<a class="soc facebook" href="' . str_replace($params, $values, $link) . '">' . $key . '</a>';
    }

    return $content . '<div id="share_target" class="social_links">' . $linksString . '</div>';
};

/**
 *  Додаємо віджет останніх постів категорії
 */

add_action('widgets_init', 'last_posts');
function last_posts (){
    register_widget('WFM_Widget');
}

class WFM_Widget extends WP_Widget{
    function __construct()
    {
        parent::__construct('FW', 'Последние записи категории', array( 'description' => 'Вывод последних записей конкретной категории'));
    }
    function widget($args,$instance)
    {
        //var_dump($instance);
        extract($args);
        extract($instance);

        echo $before_widget;
        echo $before_title.$title.$after_title;
        //echo '<div>'.$category.'</div>';

        // The Query
        $the_query = new WP_Query( array( 'cat' => $category, 'posts_per_page' => $limit, 'order'   => 'DESC' ) );

// The Loop
        if ( $the_query->have_posts() ) {
            echo '<ul>';
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                echo '<li><a href="'.get_permalink().'">' . get_the_title() . '</a></li>';
            }
            echo '</ul>';
            /* Restore original Post Data */
            wp_reset_postdata();
        } else {
            // no posts found
        }


        echo $after_widget;


    }

    function form($instance){

        //$category = null;
        $limit = 10;
        // если instance не пустой, достанем значения

        $categoryId = $this->get_field_id("category");
        $categoryName = $this->get_field_name("category");

        $limitId = $this->get_field_id("limit");
        $limitName = $this->get_field_name("limit");


        extract($instance);
        ?>
        <p>
            <label for="<?=$this->get_field_id('title');?>">Заголовок</label>
            <input type="text" name="<?=$this->get_field_name('title');?>" id="<?=$this->get_field_id('title');?>" value="<? if($title) echo esc_attr($title);?>" class="widefat">
        </p>

        <p>
            <label for="<?= $categoryId ?>"><?= __('Category', 'sg') ?></label>
        </p>
        <p>
            <select class="widefat" name="<?= $categoryName ?>" id="<?= $categoryId ?>">
                <?php $categories = get_categories(); ?>
                <?php foreach ($categories as $cat){ ?>
                    <option value="<?= $cat->cat_ID ?>"<?php if($category == $cat->cat_ID){ ?> selected="selected"<?php } ?>><?= $cat->name ?></option>
                <?php } ?>
            </select>
        </p>

        <p>
            <label for="<?= $limitId ?>"><?= __('Limit', 'sg') ?></label>
        </p>
        <p>
            <input class="widefat" type="number" min="2" id="<?= $limitId ?>" name="<?= $limitName ?>" value="<?= $limit ?>">
        </p>
        <?
    }
}

   



add_action('widgets_init', 'last_twits');
function last_twits (){
    register_widget('Twitter_Widget');
}

class Twitter_Widget extends WP_Widget{
    function __construct()
    {
        parent::__construct('Twitter', 'Последние записи твиттера', array( 'description' => 'Вывод последних записей твиттера'));
    }
    function widget($args,$instance)
    {
        //var_dump($instance);
        extract($args);
        extract($instance);

        echo $before_widget;
        echo $before_title.$title.$after_title;
        //echo '<div>'.$category.'</div>';

        // The Query
        $the_query = new WP_Query( array( 'post_type' => 'twit', 'posts_per_page' => 5, 'orderby' => 'title',  'order'   => 'DESC' ) );

// The Loop
        if ( $the_query->have_posts() ) {
            //echo '<ul>';
            echo '<div id="parent">';
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                echo "<div class='twitter_item' data-id='".get_the_title()."'>".get_the_content()."</div>";
                echo '<div style="clear:both;"></div><hr>';

            }
            echo '</div>';
           // echo '</ul>';
            /* Restore original Post Data */
            wp_reset_postdata();
        } else {
            echo 'no posts found';
        }


        echo $after_widget;


    }

    function form($instance){
	extract($instance);
        ?>
        <p>
            <label for="<?=$this->get_field_id('title');?>">Заголовок</label>
            <input type="text" name="<?=$this->get_field_name('title');?>" id="<?=$this->get_field_id('title');?>" value="<? if($title) echo esc_attr($title);?>" class="widefat">
        </p>
<?
    }
}
add_shortcode('twitter','twitter_shortcod');
function twitter_shortcod(){
    ob_start();
// The Query
$the_query = new WP_Query(array('post_type' => 'twit', 'posts_per_page' => 5, 'orderby' => 'title', 'order' => 'DESC'));

// The Loop
if ($the_query->have_posts()) {
    //echo '<ul>';
    while ($the_query->have_posts()) {
        $the_query->the_post();
        echo get_the_content();
        echo '<div style="clear:both;"></div><hr>';

    }
    // echo '</ul>';
    /* Restore original Post Data */
    wp_reset_postdata();
    $output_string= ob_get_contents();
    ob_end_clean();
    return $output_string;
} else {
    echo 'no posts found';
    $output_string= ob_get_contents();
    return $output_string;
    ob_end_clean();
}

}
add_filter('widget_text', 'do_shortcode');

/*   ********* Додавання метабоксу ************************************ */
add_action('add_meta_boxes', 'metatest_init');
add_action('save_post', 'metatest_save');

function metatest_init() {
    add_meta_box('metatest', 'MetaTest-параметр поста',
        'metatest_showup', 'post', 'normal', 'default');
}


function metatest_showup($post, $box) {

// получение существующих метаданных
    $data = get_post_meta($post->ID, '_metatest_data', true);

// скрытое поле с одноразовым кодом
    wp_nonce_field('metatest_action', 'metatest_nonce');

// поле с метаданными
    echo '<p>Метаданные: <input type="text" name="metadata_field" value="'
        . esc_attr($data) . '"/></p>';
}

function metatest_save($postID) {

// пришло ли поле наших данных?
    if (!isset($_POST['metadata_field']))
        return;

// не происходит ли автосохранение?
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

// не ревизию ли сохраняем?
    if (wp_is_post_revision($postID))
        return;

// проверка достоверности запроса
    check_admin_referer('metatest_action', 'metatest_nonce');

// коррекция данных
    $data = sanitize_text_field($_POST['metadata_field']);

// запись
    update_post_meta($postID, '_metatest_data', $data);

}


/*   ********* Додавання метабоксу ************************************ */

/** *** Ajax запросы*********************** */

add_action( 'wp_enqueue_scripts', 'myajax_data', 99 );
function myajax_data(){

    wp_localize_script('bootstrap', 'myajax',
        array(
            'url' => admin_url('admin-ajax.php')
        )
    );

}

add_action('wp_footer', 'my_action_javascript', 99); // для фронта
function my_action_javascript() {
    /*$last_id = last_id();
    if (!$last_id) $last_id=1;
    gettwits();
    $last_id2 = last_id();*/
   /* if ($last_id<>$last_id2){*/
    ?>
    <script type="text/javascript" >



        jQuery(document).ready(function($){
            setTimeout(Vivod,10000);
        function Vivod() {
            var lasttwitterid=0;
            $('.twitter_item').each(function(){
                console.log('ok <br>');
                var carrentid = parseInt($(this).attr('data-id'));
                if (carrentid>lasttwitterid){
                    lasttwitterid=carrentid;
                }
            })


                var data = {
                    action: 'my_action',
                    lastid: lasttwitterid
                };


                jQuery.post(myajax.url, data, function (data) {
                    data = jQuery.parseJSON(data);
                    //alert(data);
                    if (data.length > 0) {

                        /* Делаем проход по каждому результату, оказвашемуся в массиве,
                         где в index попадает индекс текущего элемента массива, а в data - сама статья */
                        $.each(data, function (index, data) {

                            /* Отбираем по идентификатору блок со статьями и дозаполняем его новыми данными */
                            $("#parent").prepend("<div class='twitter_item' data-id='"+ data.post_title  +"'>" + data.post_content + "</div><div style='clear:both;'></div><hr>");


                            if (data.ID > lasttwitterid) {
                                lasttwitterid = data.ID;
                            }
                        });


                    }
                });
            setTimeout(Vivod,10000);
            }

            });

    </script>
    <?php /* }*/
}

add_action('wp_ajax_my_action', 'my_action_callback');
add_action('wp_ajax_nopriv_my_action', 'my_action_callback');
function my_action_callback() {
    $last_id = $_POST['lastid'] ;
    global $wpdb;
    $newposts = $wpdb->get_results( "SELECT id, post_title, post_content FROM sg_posts WHERE post_type='twit' AND post_title>$last_id AND post_status='publish' ORDER BY post_title ASC" );
    //print_r($newposts);
    echo json_encode($newposts);
    wp_die();

}

/** *** Ajax запросы*********************** */









	










